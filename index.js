/*		ACTIVITY		*/

/*
	DIRECTIONS:
		1. Create 2 event listeners for when a user types in the first and last name inputs
		2. When this even triggers, update the span-full-names's content to show the value of the first name input on the left and right input values
		3. Stretch goal:
			► Instead of an anonymous function, create a new function that the two event listeners will call

		Guide Question: Where do the names come from and where should they go?
*/

// CREATING VARIABLES 
	let input1 = document.querySelector('#txt-first-name');
	let input2 = document.querySelector('#txt-last-name');
	let span = document.querySelector('#span-full-name');


// EVENT LISTENERS
	input1.addEventListener ('keyup', () => 
	{	realTimeUpdate() })

	input2.addEventListener ('keyup', () => 
	{	realTimeUpdate() })

	function realTimeUpdate ()
	{	console.dir(event.target.value);
		span.textContent = `Hello ${input1.value} ${input2.value}`
	}


